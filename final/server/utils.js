checkShipY = (top) => top >= ZONE_BOUNDS.top + ZONE_BOUNDS.height * 0.8
    && top <= ZONE_BOUNDS.top + ZONE_BOUNDS.height;
checkShipX = (left) => left <= ZONE_BOUNDS.left + ZONE_BOUNDS.width
    && left >= ZONE_BOUNDS.left

checkY = (top) => top >= ZONE_BOUNDS.top
    && top <= ZONE_BOUNDS.top + ZONE_BOUNDS.height;
checkX = (left) => left != (ZONE_BOUNDS.left + ZONE_BOUNDS.width)
    && left >= ZONE_BOUNDS.left

checkBodyCollision = (body_bounds, bullet) =>
    bullet.y >= body_bounds.top &&
    bullet.y <= body_bounds.top + body_bounds.height &&
    bullet.x <= body_bounds.left + body_bounds.width &&
    bullet.x >= body_bounds.left


const addBodySegment = index => {
    const piece = document.createElement("div");
    piece.className = `js-centipe-body-${index} centipe-body`
    ZONE.appendChild(piece);
}

const creatBullet = (x, y) => {
    const bullet = document.createElement("div");
    bullet.className = "js-bullet bullet";
    bullet.style.top = `${y}px`;
    bullet.style.left = `${x}px`;
    ZONE.appendChild(bullet);
}

const destroyBullet = () => {
    const bullet = document.querySelector(".js-bullet");
    if (bullet != null)
        bullet.remove();
}
const segment2Mushroom = index => {
    const segment = document.querySelector(`.js-centipe-body-${index}`);
    if (segment != null) {
        const segment_bounds = segment.getBoundingClientRect();
        const mushroom = document.createElement("div");
        mushroom.className = "js-mushroom mushroom";
        mushroom.style.top = `${segment_bounds.top}px`;
        mushroom.style.left = `${segment_bounds.left}px`;
        segment.remove();
        ZONE.appendChild(mushroom);
    }
}
const head2Mushroom = () => {
    const head = document.querySelector(`.js-centipe-head`);
    if (head != null) {
        const head_bounds = head.getBoundingClientRect();
        const mushroom = document.createElement("div");
        mushroom.className = "js-mushroom mushroom";
        mushroom.style.top = `${head_bounds.top}px`;
        mushroom.style.left = `${head_bounds.left}px`;
        head.remove();
        ZONE.appendChild(mushroom);
    }
}

const updateScore = score => {
    console.log("prro")
    const elemScore = document.querySelector(".js-score-count")
    elemScore.innerHTML = score
}