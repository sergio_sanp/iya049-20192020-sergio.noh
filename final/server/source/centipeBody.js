const slidingArray = (arraySize, initialState) => {
    const body = initialState;
    return {
        push: value => {
            if (arraySize === 0)
                return slidingArray(arraySize, body);

            if (body.length >= arraySize)
                body.shift()
            body.push(value)
            return slidingArray(arraySize, body);
        },
        pop: () => {
            body.pop();
            return slidingArray(arraySize - 1, body);
        },
        getArraySize: () => {
            return arraySize;
        },
        getValue: () => [...body]
    }
}