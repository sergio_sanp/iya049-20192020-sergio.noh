const { Stream, redux } = window;

const moveWithMouse = new Stream(next => {
    ZONE.addEventListener("mousemove", next);
})

moveWithMouse.subscribe(e => {
    const { x, y } = e;
    if (checkShipY(y)) SHIP.style.top = `${y - 10}px`;
    if (checkShipX(x)) SHIP.style.left = `${x - 10}px`;
})

const ticksCentipe = new Stream(next => {
    setInterval(() => next(), INTIAL_CLOCK_SPEED);
});

const TICK = "__TICK__";
const CLICK = "__CLICK__";
const ticksBullet = new Stream(next => {
    setInterval(() => next(TICK), BULLET_SPEED);
});

const click = new Stream(next => {
    ZONE.addEventListener("click", next);
})
const mappedClick = click.map(() => CLICK)

const initialState = {
    top: CENTIPE_BOUNDS.y,
    left: CENTIPE_BOUNDS.x,
    direction: 1,
    UpDown: 1,
    body: slidingArray(
        CENTIPE_SIZE,
        []
    ),
}

const scoreFunc = () => {
    let score = 0;
    return {
        getScore: () => score,
        addScore: add => score = score + add,
        updateScoreInDOM: () => updateScore(score)
    }

}
const scoreHandler = scoreFunc();

function updatePosition(state = initialState, action) {
    state.body = state.body.push({ top: state.top, left: state.left });
    switch (action.type) {
        case 'UP_DOWN': {
            const position = state.top + CENTIPE_VELOCITY * state.UpDown;
            return {
                ...state,
                top: position,
            }
        }
        case 'HORIZONTAL': {
            const position = state.left + CENTIPE_VELOCITY * state.direction
            return {
                ...state,
                left: position,
            }
        }
        case 'CHANGE_DIRECTION':
            return { ...state, direction: state.direction * -1 }
        case 'CHANGE_DIRECTION_UP_DOWN':
            return { ...state, UpDown: state.UpDown * -1 }
        case 'DESTROY_SEGMENT': {
            state.body = state.body.pop()
            return { ...state }
        }
        case 'DESTROY_HEAD': {
            return { body: state.body }
        }
        case 'RESTART':
            return { ...initialState }
        default:
            return state
    }
}



const position = redux.createStore(updatePosition);
position.subscribe(() => {
    const { top, left, body, direction } = position.getState();
    CENTIPE.style.top = `${top}px`;
    CENTIPE.style.left = `${left}px`;
    CENTIPE.className = `js-centipe-head centipe-head ${direction === 1 ? "head-r" : "head-l"}`;
    bodyValue = body.getValue()
    bodyValue.forEach((value, index) => {
        const bodyDOM = document.querySelector(`.js-centipe-body-${index}`)
        if (bodyDOM === null) {
            addBodySegment(index)
        }
        else if (body.getValue().length >= 1) {
            bodyDOM.style.top = `${value.top}px`;
            bodyDOM.style.left = `${value.left}px`;
        }
    });
})

ticksCentipe.subscribe(() => {
    const { left, top, direction, UpDown } = position.getState();
    if (!checkY(top + (CENTIPE_VELOCITY * UpDown))) {
        position.dispatch({ type: 'CHANGE_DIRECTION_UP_DOWN' })
    }
    if (!checkX(left + (CENTIPE_VELOCITY * direction))) {
        position.dispatch({ type: 'CHANGE_DIRECTION' })
        position.dispatch({ type: 'UP_DOWN' })
    } else
        position.dispatch({ type: 'HORIZONTAL' })
})

function checkCollision(state, bullet) {
    bodyValue = state.body.getValue();
    for (const [index, value] of bodyValue.entries()) {
        const bodyDOM = document.querySelector(`.js-centipe-body-${index}`)
        if (bodyDOM != 'undefined') {
            let body_bounds = bodyDOM.getBoundingClientRect();
            if (checkBodyCollision(body_bounds, bullet)) {
                destroyBullet();
                return true
            }
        }
    }
    return false
}
function checkHeadCollision(bullet) {
    const head = document.querySelector(".js-centipe-head");
    const head_bounds = head.getBoundingClientRect();
    if (checkBodyCollision(head_bounds, bullet)) {
        destroyBullet();
        return true
    }
}

const bullet = Stream.merge(mappedClick, ticksBullet)
    .withLatestFrom(moveWithMouse)
    .scan(
        (bullet, [mouseEvent, event]) => {
            state = position.getState();
            if (event === TICK) {
                if (!checkY(bullet.y)) {
                    destroyBullet();
                    return {}
                }
                else if (bullet.x != undefined) {
                    if (checkCollision(state, bullet)) {
                        position.dispatch({ type: 'DESTROY_SEGMENT' })
                        segment2Mushroom(state.body.getArraySize());
                        scoreHandler.addScore(BODY_HIT_SCORE)
                        scoreHandler.updateScoreInDOM();
                        return {}
                    }
                    if (checkHeadCollision(bullet) && state.body.getValue().length === 0) {
                        position.dispatch({ type: 'DESTROY_HEAD' })
                        head2Mushroom();
                        scoreHandler.addScore(KILL_HIT_SCORE)
                        scoreHandler.updateScoreInDOM();
                        // position.dispatch({ type: 'RESTART' })
                        return {}
                    }
                    if (checkHeadCollision(bullet)) {
                        position.dispatch({ type: 'DESTROY_SEGMENT' })
                        segment2Mushroom(state.body.getArraySize());
                        scoreHandler.addScore(HEAT_HIT_SCORE)
                        scoreHandler.updateScoreInDOM();
                        return {}
                    }
                    return {
                        x: bullet.x,
                        y: bullet.y - 1
                    }
                }
                return {}
            }
            if (event === CLICK) {
                if (JSON.stringify(bullet) === "{}") {
                    creatBullet(mouseEvent.x, mouseEvent.y)
                    return { x: mouseEvent.x, y: mouseEvent.y };
                }
                return bullet
            }
            throw Error("emitted value must be tick or click");
        },
        {}
    );

bullet.subscribe((bullet) => {
    if (bullet.x != undefined) {
        const bulletNew = document.querySelector(".js-bullet");
        if (bulletNew != null)
            bulletNew.style.top = `${bullet.y}px`;
    }
})