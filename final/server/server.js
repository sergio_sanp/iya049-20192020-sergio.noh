const http = require("http")
const url = require('url')
const fs = require('fs')
http
    .createServer(function (request, response) {
        if (request.url === "/") {
            response.setHeader("Content-type", "text/html");
            const home = fs.readFileSync("./index.html");
            response.end(home);
        }
        fs.readFile("./" + request.url, function (err, data) {
            if (!err) {
                var dotoffset = request.url.lastIndexOf(".");
                var mimetype =
                    dotoffset == -1
                        ? "text/plain"
                        : {
                            ".html": "text / html",
                            ".ico": "image/x-icon",
                            ".jpg": "image/jpeg",
                            ".png": "image/png",
                            ".gif": "image/gif",
                            ".css": "text/CSS",
                            ".js": "text/javascript"
                        }[request.url.substr(dotoffset)];

                response.setHeader("Content-type", mimetype);
                response.end(data);
                console.log(request.url, mimetype);
            } else
                console.log("file not found : " + request.url);
            const home = fs.readFileSync("./index.html");
            response.end(home);

        });
    })
    .listen(8080, "localhost");