const INTIAL_CLOCK_SPEED = 120;
const BULLET_SPEED = 0.5;

const BOARD_WIDTH = 600;
const BOARD_HEIGHT = 500;

const CENTIPE_SIZE = 10;
const CENTIPE_VELOCITY = 20;

const KILL_HIT_SCORE = 30
const HEAT_HIT_SCORE = 20
const BODY_HIT_SCORE = 10

const ZONE = document.querySelector(".js-game-zone");
const ZONE_BOUNDS = ZONE.getBoundingClientRect();

const SHIP = document.querySelector(".js-ship");

const CENTIPE = document.querySelector(".js-centipe");
const CENTIPE_BOUNDS = SHIP.getBoundingClientRect();


