## Integrantes del grupo.

... ✏️
* Fernández Díez, Álvaro Miguel
* Díez Fernández, Miguel Álvaro

## Desglose de tareas

* Implementar la función `foobar`. 

 Asignado a Munguía Hoyo, Gerardo

* Implementar la función `bazbaz`.

 Asignado a Mungúia Hoyo, Gerardo
